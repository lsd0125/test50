<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="js/jquery-3.2.1.js"></script>
    <title>Document</title>
</head>
<body>

<table>
    <?php for($blue=0; $blue<=255; $blue+=17): ?>
    <tr>
        <?php for($green=0; $green<=255; $green+=17):
            $t = sprintf("%04x", ($green<<8) + $blue);
            ?>
        <td bgcolor="#00<?= $t ?>">#00<?= $t ?></td>
        <?php endfor ?>
    </tr>
    <?php endfor ?>
</table>




</body>
</html>