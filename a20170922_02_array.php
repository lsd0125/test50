<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="js/jquery-3.2.1.js"></script>
    <title>Document</title>
</head>
<body>
<?php
$ar = array(3,4,5,
    'name' =>'david'
    );
$ar[] = 'hello';
$ar[] = 'hi';

echo '<pre>';
print_r($ar);
echo '</pre>';

echo count($ar). "<br>";
echo "-----------------<br>";

foreach($ar as $a){
    echo " $a <br>";
}

echo "-----------------<br>";
foreach($ar as $k=>$v){
    echo "$k : $v : ". $ar[$k]. "<br>";
}


?>
</body>
</html>