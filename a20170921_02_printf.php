<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php

$a = 'Victor';

print("Hello, $a <br>");

printf("Hello, %s <br>", $a);

$n = 5;
$m = 7;

printf("%s * %s = %s<br>", $n, $m, $n*$m);


?>


</body>
</html>