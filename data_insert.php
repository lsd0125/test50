<?php
require __DIR__. '/__connect_db.php';
$title = '新增資料';
$page_name = 'data_insert';

if(isset($_POST['name'])) {

    $sql = "INSERT INTO `address_book`(
        `name`, `mobile`,
        `email`, `birthday`, `address`
        ) VALUES (
        ?, ?, ?, ?, ?
        )";

    $stmt = $mysqli->prepare($sql);

    $stmt->bind_param('sssss',
        $_POST['name'],
        $_POST['mobile'],
        $_POST['email'],
        $_POST['birthday'],
        $_POST['address']

        );

    $stmt->execute();

    $affected_rows = $stmt->affected_rows;


    /*

    $sql = sprintf("INSERT INTO `address_book`(
        `name`, `mobile`,
        `email`, `birthday`, `address`
        ) VALUES (
        '%s', '%s',
        '%s', '%s', '%s'
        )",
        $mysqli->escape_string($_POST['name']),
        $mysqli->escape_string($_POST['mobile']),
        $mysqli->escape_string($_POST['email']),
        $mysqli->escape_string($_POST['birthday']),
        $mysqli->escape_string($_POST['address'])
        );

    $result = $mysqli->query($sql);
//    echo $sql;
//    echo '<br>';
//    echo $result ? 't' : 'f';
    */
}


?>
<?php include __DIR__. '/__html_head.php'; ?>
    <style>
        .form-group > small {
            color: red;
            display: none;
        }
    </style>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>

    <?php if(isset($affected_rows)): ?>
        <?php if($affected_rows==1): ?>
            <div class="alert alert-success" role="alert" style="margin-top: 30px">
                資料新增完成
            </div>
        <?php else: ?>
            <div class="alert alert-danger" role="alert" style="margin-top: 30px">
                新增發生錯誤
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <div class="row justify-content-md-center" style="margin-top: 30px">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">
                新增資料 <?= isset($affected_rows) ? $affected_rows : ''; ?>
            </div>
            <div class="card-body">
                <!-- `name`, `mobile`, `email`, `birthday`, `address`  -->
                <form name="form1" method="post" onsubmit="return checkForm()">
                    <div class="form-group">
                        <label for="name">姓名</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="">
                         <small id="nameHelp" class="form-text">請填姓名</small>
                    </div>
                    <div class="form-group">
                        <label for="mobile">手機</label>
                        <input type="text" class="form-control" id="mobile" name="mobile" placeholder="">
                        <small id="mobileHelp" class="form-text">請填十碼手機號碼</small>
                    </div>
                    <div class="form-group">
                        <label for="email">電郵</label>
                        <input type="text" class="form-control" id="email" name="email" placeholder="">
                        <small id="emailHelp" class="form-text">請填入正確電子郵箱格式</small>
                    </div>
                    <div class="form-group">
                        <label for="birthday">生日</label>
                        <input type="text" class="form-control" id="birthday" name="birthday" placeholder="">
                        <small id="birthdayHelp" class="form-text">請填生日</small>
                    </div>
                    <div class="form-group">
                        <label for="address">地址</label>
                        <textarea  class="form-control" name="address" id="address" cols="50" rows="3" placeholder="地址"></textarea>
                        <small id="addressHelp" class="form-text">請填地址</small>
                    </div>


                    <button type="submit" class="btn btn-primary">新增</button>
                </form>


            </div>
        </div>

    </div>
    </div>
</div>
    <script>
        var field_names = ['name', 'mobile', 'email', 'birthday', 'address'];
        var fields = {};
        var i, s, key;
        var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        var mobile_pattern = /[0-9\-]{10,}/;

        for(i=0; i< field_names.length; i++){
            key = field_names[i];
            fields[key] = {
                input: $('#'+key),
                help: $('#'+key+'Help')
            };
        }

        console.log(fields);

        function checkForm(){
            var isPass = true;

            for(s in fields){
                fields[s].help.hide();
                fields[s].input.css('border-color', 'rgba(0,0,0,.15)');
            }


            if(fields['name'].input.val().length < 2){
                // alert('請填姓名');
                fields['name'].help.show();
                fields['name'].input.css('border-color', 'red');
                isPass = false;

            }
            if(! mobile_pattern.test(fields['mobile'].input.val())){
                fields['mobile'].help.show();
                fields['mobile'].input.css('border-color', 'red');
                isPass = false;

            }
            if(! pattern.test( fields['email'].input.val() )){
                fields['email'].help.show();
                fields['email'].input.css('border-color', 'red');
                isPass = false;

            }




            return isPass;
        }
    </script>
<?php include __DIR__. '/__html_foot.php'; ?>