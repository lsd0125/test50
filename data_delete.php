<?php
require __DIR__. '/__connect_db.php';

echo '-#1-';

if(isset($_GET['sid'])){
    $sid = intval($_GET['sid']);
    $sql = "DELETE FROM `address_book` WHERE `sid`=?";
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param('i', $sid);
    $stmt->execute();

    echo $stmt->affected_rows;

    echo '-#2-';

}
echo '-#3-';
if(isset($_SERVER['HTTP_REFERER'])){
    header("Location: ". $_SERVER['HTTP_REFERER']);
} else {
    header("Location: data_list.php");
}
