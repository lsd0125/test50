<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="js/jquery-3.2.1.js"></script>
    <title>Document</title>
</head>
<body>
<form onsubmit="return beforeSubmit();">

    <input type="text" name="a" id="a"><br>
    <input type="text" name="b" id="b"><br>
    <input type="submit">


</form>
<div id="info"></div>

<script>
    function beforeSubmit() {
        var a = $('#a').val();
        var b = $('#b').val();

        $.get('a20170921_06_get.php', {a:a, b:b}, function(data){

            $('#info').html(data);
        });


        return false;
    }


</script>
</body>
</html>