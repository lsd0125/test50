<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="js/jquery-3.2.1.js"></script>
    <title>Document</title>
</head>
<body>
<?php
$ar = array(3,4,5,6,7);
$ar2 = [8,9,10];

echo '<pre>';
print_r($ar);
echo '</pre>';

echo '<pre>';
var_dump($ar);
echo '</pre>';

$ar3 = array(
        'name' => 'peter',
        'age' => 28,
);
$ar3 = [
    'name' => 'peter',
    'age' => 28,
];

echo '<pre>';
print_r($ar3);
echo '</pre>';

?>
</body>
</html>