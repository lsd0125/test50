<?php
$a = "hello";
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">

    <script src="js/jquery-3.2.1.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>

    <title>Document</title>
</head>
<body>

<div class="container">
    <?php include __DIR__. '/a20170925_05_navbar.php'; ?>
    <?php //include_once __DIR__. '/a20170925_05_navbar.php'; ?>
    <?php //require __DIR__. '/a20170925_05_navbar.php'; ?>
    <?php //require_once __DIR__. '/a20170925_05_navbar.php'; ?>

    <div class="card">

        <form>
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                       placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.
                </small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input type="checkbox" class="form-check-input">
                    Check me out
                </label>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

</div>


</body>
</html>