<?php
require __DIR__. '/__connect_db.php';
$title = '列表';
$page_name = 'data_list';

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
$page = $page <= 0 ? 1 : $page;
// $page <= 0 ? $page=1 : '';

$per_page = 5;

$t_sql = "SELECT COUNT(1) FROM `address_book`";
$t_rs = $mysqli->query($t_sql);
$total_rows = $t_rs->fetch_row()[0];

$pages = ceil($total_rows/$per_page);



$sql = sprintf("SELECT * FROM `address_book` ORDER BY `sid` DESC  LIMIT %s, %s",
    ($page-1)*$per_page,
    $per_page
);


$rs = $mysqli->query($sql);

// $row = $rs->fetch_assoc();


?>
<?php include __DIR__. '/__html_head.php'; ?>

    <style>
        .my-remove a{
            color: red;
            font-size: large;
        }
        .my-edit a{
            color: #171eff;
            font-size: large;
        }

    </style>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>
    <div class="row justify-content-md-center" style="margin-top: 20px">
        <div class="col-md-auto">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item <?= $page==1 ? 'disabled' : '' ?>">
                        <a class="page-link" href="?page=1">
                            <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                        </a></li>


                    <li class="page-item <?= $page==1 ? 'disabled' : '' ?>">
                        <a class="page-link" href="?page=<?= $page-1 ?>">
                            <i class="fa fa-angle-left" aria-hidden="true"></i>
                        </a></li>


                    <li class="page-item "><a class="page-link"><?= $page. " / ". $pages ?></a></li>

                    <li class="page-item <?= $page==$pages ? 'disabled' : '' ?>">
                        <a class="page-link" href="?page=<?= $page+1 ?>">
                            <i class="fa fa-angle-right" aria-hidden="true"></i>
                        </a></li>

                    <li class="page-item <?= $page==$pages ? 'disabled' : '' ?>">
                        <a class="page-link" href="?page=<?= $pages ?>">
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                        </a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 20px">
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>刪除</th>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>Birthday</th>
            <th>Address</th>
            <th>編輯</th>
        </tr>
        </thead>
        <tbody>
            <?php while( $row = $rs->fetch_assoc() ): ?>
            <tr>
                <td class="my-remove">
                    <a href="javascript:delete_it(<?= $row['sid'] ?>)">
                        <i class="fa fa-remove"></i>
                    </a>
                </td>
                <td><?= $row['sid'] ?></td>
                <td><?= $row['name'] ?></td>
                <td><?= $row['email'] ?></td>
                <td><?= $row['mobile'] ?></td>
                <td><?= $row['birthday'] ?></td>
                <td><?= htmlentities($row['address']) ?></td>
<!--                <td>--><?//= strip_tags($row['address']) ?><!--</td>-->

                <td class="my-edit">
                    <a href="data_edit.php?sid=<?= $row['sid'] ?>">
                        <i class="fa fa-edit"></i>
                    </a>
                </td>
            </tr>
            <?php endwhile; ?>
        </tbody>
    </table>
    </div>
    <div class="row justify-content-md-center" style="margin-top: 20px">
        <div class="col-md-auto">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
                    <?php for($i=1; $i<=$pages; $i++): ?>
                    <li class="page-item <?= $page==$i ? 'disabled' : '' ?>">
                        <a class="page-link" href="?page=<?= $i ?>">
                            <?= $i ?>
                        </a>
                    </li>
                    <?php endfor; ?>
                </ul>
            </nav>
        </div>
    </div>
</div>
    <script>
        function delete_it(sid){
            if(confirm('確定要刪除編號為'+ sid +'的資料?')){
                location.href = 'data_delete.php?sid=' + sid;
            }

        }
    </script>
<?php include __DIR__. '/__html_foot.php'; ?>